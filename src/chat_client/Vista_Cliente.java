package chat_client;

import java.net.*;
import java.io.*;
import java.util.*;



public class Vista_Cliente extends javax.swing.JFrame 
{
    String usuario, direccion = "localhost";
    ArrayList<String> usuarios = new ArrayList();
    int port = 2222;
    Boolean isConnected = false;
    
    Socket sock;
    BufferedReader reader;
    PrintWriter writer;
    
    public void ListenThread() 
    {
         Thread IncomingReader = new Thread(new IncomingReader());
         IncomingReader.start();
    }
    
    
    public void AgregarUsuario(String data) 
    {
         usuarios.add(data);
    }
    
    
    public void RemoverUsuario(String data) 
    {
         ta_chat.append(data + " esta desconectado.\n");
    }
    
    
    public void EscribirUsuarios() 
    {
         String[] tempList = new String[(usuarios.size())];
         usuarios.toArray(tempList);
         for (String token:tempList) 
         {
             //users.append(token + "\n");
         }
    }
    
    
    public void EnviarDesconectado() 
    {
        String bye = (usuario + ": :Desconectado");
        try
        {
            writer.println(bye); 
            writer.flush(); 
        } catch (Exception e) 
        {
            ta_chat.append("No se puedo enviar el mensaje de desconectado.\n");
        }
    }

    
    public void Desconectado() 
    {
        try 
        {
            ta_chat.append("Desconectado.\n");
            sock.close();
        } catch(Exception ex) {
            ta_chat.append("Fallo al desconectar. \n");
        }
        isConnected = false;
        tf_usuario.setEditable(true);

    }
    
    public Vista_Cliente() 
    {
        initComponents();
        setLocationRelativeTo(null);
    }
    
    public class IncomingReader implements Runnable
    {
        @Override
        public void run() 
        {
            String[] data;
            String stream, done = "Terminado", connect = "Conectado", disconnect = "Desconectado", chat = "Chat";

            try 
            {
                while ((stream = reader.readLine()) != null) 
                {
                     data = stream.split(":");

                     if (data[2].equals(chat)) 
                     {
                        ta_chat.append(data[0] + ": " + data[1] + "\n");
                        ta_chat.setCaretPosition(ta_chat.getDocument().getLength());
                     } 
                     else if (data[2].equals(connect))
                     {
                        ta_chat.removeAll();
                        AgregarUsuario(data[0]);
                     } 
                     else if (data[2].equals(disconnect)) 
                     {
                         RemoverUsuario(data[0]);
                     } 
                     else if (data[2].equals(done)) 
                     {
                        //users.setText("");
                        EscribirUsuarios();
                        usuarios.clear();
                     }
                }
           }catch(Exception ex) { }
        }
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lb_direccion = new javax.swing.JLabel();
        tf_direccion = new javax.swing.JTextField();
        lb_puerto = new javax.swing.JLabel();
        tf_puerto = new javax.swing.JTextField();
        lb_usuario = new javax.swing.JLabel();
        tf_usuario = new javax.swing.JTextField();
        lb_contraseña = new javax.swing.JLabel();
        tf_contraseña = new javax.swing.JTextField();
        b_conectarse = new javax.swing.JButton();
        b_desconectarse = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        ta_chat = new javax.swing.JTextArea();
        tf_chat = new javax.swing.JTextField();
        b_enviar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("TecnoChat - Cliente");
        setBackground(new java.awt.Color(255, 204, 204));
        setIconImage(getIconImage());
        setName("client"); // NOI18N
        setResizable(false);

        lb_direccion.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N
        lb_direccion.setText("Direccion:");

        tf_direccion.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N
        tf_direccion.setText("localhost");
        tf_direccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tf_direccionActionPerformed(evt);
            }
        });

        lb_puerto.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N
        lb_puerto.setText("Puerto:");

        tf_puerto.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N
        tf_puerto.setText("2222");
        tf_puerto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tf_puertoActionPerformed(evt);
            }
        });

        lb_usuario.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N
        lb_usuario.setText("Usuario:");

        tf_usuario.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N
        tf_usuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tf_usuarioActionPerformed(evt);
            }
        });

        lb_contraseña.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N
        lb_contraseña.setText("Contraseña:");

        tf_contraseña.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N

        b_conectarse.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N
        b_conectarse.setText("Conectarse");
        b_conectarse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_conectarseActionPerformed(evt);
            }
        });

        b_desconectarse.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N
        b_desconectarse.setText("Desconectarse");
        b_desconectarse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_desconectarseActionPerformed(evt);
            }
        });

        ta_chat.setBackground(new java.awt.Color(255, 204, 255));
        ta_chat.setColumns(20);
        ta_chat.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N
        ta_chat.setRows(5);
        jScrollPane1.setViewportView(ta_chat);

        tf_chat.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N

        b_enviar.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N
        b_enviar.setText("Enviar");
        b_enviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_enviarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(tf_chat, javax.swing.GroupLayout.PREFERRED_SIZE, 352, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(b_enviar, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE))
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lb_usuario, javax.swing.GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
                            .addComponent(lb_direccion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(tf_direccion, javax.swing.GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE)
                            .addComponent(tf_usuario))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lb_contraseña, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lb_puerto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(tf_contraseña)
                            .addComponent(tf_puerto, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(b_conectarse)
                        .addGap(2, 2, 2)
                        .addComponent(b_desconectarse)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lb_direccion)
                    .addComponent(tf_direccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lb_puerto)
                    .addComponent(tf_puerto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(tf_usuario)
                    .addComponent(tf_contraseña)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lb_usuario)
                        .addComponent(lb_contraseña)
                        .addComponent(b_conectarse)
                        .addComponent(b_desconectarse)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tf_chat)
                    .addComponent(b_enviar, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE))
                .addGap(22, 22, 22))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tf_direccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tf_direccionActionPerformed
       
    }//GEN-LAST:event_tf_direccionActionPerformed

    private void tf_puertoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tf_puertoActionPerformed
   
    }//GEN-LAST:event_tf_puertoActionPerformed

    private void tf_usuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tf_usuarioActionPerformed
    
    }//GEN-LAST:event_tf_usuarioActionPerformed

    private void b_conectarseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_conectarseActionPerformed
        if (isConnected == false) 
        {
            usuario = tf_usuario.getText();
            tf_usuario.setEditable(false);

            try 
            {
                sock = new Socket(direccion, port);
                InputStreamReader streamreader = new InputStreamReader(sock.getInputStream());
                reader = new BufferedReader(streamreader);
                writer = new PrintWriter(sock.getOutputStream());
                writer.println(usuario + ":se ha conectado.:Conectado");
                writer.flush(); 
                isConnected = true; 
            } 
            catch (Exception ex) 
            {
                ta_chat.append("No se puede conectar. Intente de nuevo. \n");
                tf_usuario.setEditable(true);
            }
            
            ListenThread();
            
        } else if (isConnected == true) 
        {
            ta_chat.append("Ya estas conectado. \n");
        }
    }//GEN-LAST:event_b_conectarseActionPerformed

    private void b_desconectarseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_desconectarseActionPerformed
        EnviarDesconectado();
        Desconectado();
    }//GEN-LAST:event_b_desconectarseActionPerformed

    private void b_enviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_enviarActionPerformed
        String nothing = "";
        if ((tf_chat.getText()).equals(nothing)) {
            tf_chat.setText("");
            tf_chat.requestFocus();
        } else {
            try {
               writer.println(usuario + ":" + tf_chat.getText() + ":" + "Chat");
               writer.flush(); // flushes the buffer
            } catch (Exception ex) {
                ta_chat.append("El mensaje no se envio. \n");
            }
            tf_chat.setText("");
            tf_chat.requestFocus();
        }

        tf_chat.setText("");
        tf_chat.requestFocus();
    }//GEN-LAST:event_b_enviarActionPerformed

    public static void main(String args[]) 
    {
        java.awt.EventQueue.invokeLater(new Runnable() 
        {
            @Override
            public void run() 
            {
                new Vista_Cliente().setVisible(true);
            }
        });
    }
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton b_conectarse;
    private javax.swing.JButton b_desconectarse;
    private javax.swing.JButton b_enviar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lb_contraseña;
    private javax.swing.JLabel lb_direccion;
    private javax.swing.JLabel lb_puerto;
    private javax.swing.JLabel lb_usuario;
    private javax.swing.JTextArea ta_chat;
    private javax.swing.JTextField tf_chat;
    private javax.swing.JTextField tf_contraseña;
    private javax.swing.JTextField tf_direccion;
    private javax.swing.JTextField tf_puerto;
    private javax.swing.JTextField tf_usuario;
    // End of variables declaration//GEN-END:variables


}
