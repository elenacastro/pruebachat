package chat_server;

import java.io.*;
import java.net.*;
import java.util.*;

public class Vista_Server extends javax.swing.JFrame 
{
   ArrayList clientOutputStreams;
   ArrayList<String> usuarios;

   public class ClientHandler implements Runnable	
   {
       BufferedReader reader;
       Socket sock;
       PrintWriter cliente;

       public ClientHandler(Socket clientSocket, PrintWriter user) 
       {
            cliente = user;
            try 
            {
                sock = clientSocket;
                InputStreamReader isReader = new InputStreamReader(sock.getInputStream());
                reader = new BufferedReader(isReader);
            }
            catch (Exception ex) 
            {
                ta_chat.append("Error inesperado... \n");
            }

       }

       @Override
       public void run() 
       {
            String message, connect = "Conectado", disconnect = "Desconectado", chat = "Chat" ;
            String[] data;

            try 
            {
                while ((message = reader.readLine()) != null) 
                {
                    ta_chat.append("Recibido: " + message + "\n");
                    data = message.split(":");
                    
                    for (String token:data) 
                    {
                        ta_chat.append(token + "\n");
                    }

                    if (data[2].equals(connect)) 
                    {
                        DecirATodos((data[0] + ":" + data[1] + ":" + chat));
                        AgregarUsuario(data[0]);
                    } 
                    else if (data[2].equals(disconnect)) 
                    {
                        DecirATodos((data[0] + ":se desconecto." + ":" + chat));
                        Remover_Usuario(data[0]);
                    } 
                    else if (data[2].equals(chat)) 
                    {
                        DecirATodos(message);
                    } 
                    else 
                    {
                        ta_chat.append("No hubo condiciones. \n");
                    }
                } 
             } 
             catch (Exception ex) 
             {
                ta_chat.append("Se perdio la conexion. \n");
                ex.printStackTrace();
                clientOutputStreams.remove(cliente);
             } 
	} 
    }

    public Vista_Server() 
    {
        initComponents();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        ta_chat = new javax.swing.JTextArea();
        b_empezar = new javax.swing.JButton();
        b_terminar = new javax.swing.JButton();
        b_usuarios = new javax.swing.JButton();
        b_limpiar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("TecnoChat - Servidor");
        setBackground(new java.awt.Color(204, 255, 255));
        setName("server"); // NOI18N
        setResizable(false);

        ta_chat.setBackground(new java.awt.Color(204, 204, 255));
        ta_chat.setColumns(20);
        ta_chat.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N
        ta_chat.setRows(5);
        jScrollPane1.setViewportView(ta_chat);

        b_empezar.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N
        b_empezar.setText("Empezar");
        b_empezar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_empezarActionPerformed(evt);
            }
        });

        b_terminar.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N
        b_terminar.setText("Terminar");
        b_terminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_terminarActionPerformed(evt);
            }
        });

        b_usuarios.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N
        b_usuarios.setText("Usuarios en linea");
        b_usuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_usuariosActionPerformed(evt);
            }
        });

        b_limpiar.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 11)); // NOI18N
        b_limpiar.setText("Limpiar");
        b_limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_limpiarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(b_terminar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(b_empezar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 257, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(b_limpiar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(b_usuarios, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 340, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(b_empezar)
                    .addComponent(b_usuarios))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(b_limpiar)
                    .addComponent(b_terminar))
                .addGap(20, 20, 20))
        );

        getAccessibleContext().setAccessibleName("Chat - Servidor");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void b_terminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_terminarActionPerformed
        try 
        {
            Thread.sleep(5000);                 
        } 
        catch(InterruptedException ex) {Thread.currentThread().interrupt();}
        
        DecirATodos("Servidor: Se detuvo y todos los usuarios seran desconectados.\n:Chat");
        ta_chat.append("Deteniendo el servidor... \n");
        
        ta_chat.setText("");
        
    }//GEN-LAST:event_b_terminarActionPerformed

    private void b_empezarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_empezarActionPerformed
        Thread starter = new Thread(new IniciarServer());
        starter.start();
        
        ta_chat.append("Iniciando servidor...\n");
    }//GEN-LAST:event_b_empezarActionPerformed

    private void b_usuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_usuariosActionPerformed
        ta_chat.append("\n Usuarios online : \n");
        for (String usuario_actual : usuarios)
        {
            ta_chat.append(usuario_actual);
            ta_chat.append("\n");
        }    
        
    }//GEN-LAST:event_b_usuariosActionPerformed

    private void b_limpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_limpiarActionPerformed
        ta_chat.setText("");
    }//GEN-LAST:event_b_limpiarActionPerformed

    public static void main(String args[]) 
    {
        java.awt.EventQueue.invokeLater(new Runnable() 
        {
            @Override
            public void run() {
                new Vista_Server().setVisible(true);
            }
        });
    }
    
    public class IniciarServer implements Runnable 
    {
        @Override
        public void run() 
        {
            clientOutputStreams = new ArrayList();
            usuarios = new ArrayList();  

            try 
            {
                ServerSocket serverSock = new ServerSocket(2222);

                while (true) 
                {
				Socket clientSock = serverSock.accept();
				PrintWriter writer = new PrintWriter(clientSock.getOutputStream());
				clientOutputStreams.add(writer);

				Thread listener = new Thread(new ClientHandler(clientSock, writer));
				listener.start();
				ta_chat.append("Logro la conexion. \n");
                }
            }
            catch (Exception ex)
            {
                ta_chat.append("Error al hacer la conexion. \n");
            }
        }
    }
    
    public void AgregarUsuario (String data) 
    {
        String message, add = ": :Conectado", done = "Servidor: :terminado", name = data;
        ta_chat.append("Antes " + name + " agregado. \n");
        usuarios.add(name);
        ta_chat.append("Despues " + name + " agregado. \n");
        String[] tempList = new String[(usuarios.size())];
        usuarios.toArray(tempList);

        for (String token:tempList) 
        {
            message = (token + add);
            DecirATodos(message);
        }
        DecirATodos(done);
    }
    
    public void Remover_Usuario (String data) 
    {
        String message, add = ": :Conectando", done = "Servidor: :Terminado", name = data;
        usuarios.remove(name);
        String[] tempList = new String[(usuarios.size())];
        usuarios.toArray(tempList);

        for (String token:tempList) 
        {
            message = (token + add);
            DecirATodos(message);
        }
        DecirATodos(done);
    }
    
    public void DecirATodos(String message) 
    {
	Iterator it = clientOutputStreams.iterator();

        while (it.hasNext()) 
        {
            try 
            {
                PrintWriter writer = (PrintWriter) it.next();
		writer.println(message);
		ta_chat.append("Enviando: " + message + "\n");
                writer.flush();
                ta_chat.setCaretPosition(ta_chat.getDocument().getLength());

            } 
            catch (Exception ex) 
            {
		ta_chat.append("Error al enviarlo a todos. \n");
            }
        } 
    }
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton b_empezar;
    private javax.swing.JButton b_limpiar;
    private javax.swing.JButton b_terminar;
    private javax.swing.JButton b_usuarios;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea ta_chat;
    // End of variables declaration//GEN-END:variables

   

}


